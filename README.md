# RSS Anime Notifier

Get anime schedule from https://anichart.net and gives an RSS feed to episodes that have already aired

### How to Run
1. Install `python3` (because this is in python)  
2. `pip3 install -r requirements.txt`  
3. `python3 rss-anime-notifier.py`  

### Environment Variables
`PORT` - Specify the port to listen to, default: 8080

### How to Use
1. Start the script, obviously  
2. Get the Anilist anime ID  
   For example, if the URL is https://anilist.co/anime/112609/Majo-no-Tabitabi, the anime ID is 112609  
3. Add http://127.0.0.1:[port]/[animeID] to your RSS reader  
   For example, if your port is 8080 and the anime ID is 112609, you'd use http://127.0.0.1:8080/112609
