import os
import json
import time
import logging
from datetime import datetime
import aiohttp
import aiohttp.web
import rfeed

PORT = os.environ.get('PORT', 8080)
ANILIST_QUERY = '''query ($id: Int) {
  Media (id: $id) {
    title {
      romaji
      english
    }
    airingSchedule {
      nodes {
        id
        airingAt
        episode
      }
    }
    episodes
    siteUrl
  }
}'''

logging.basicConfig(level=logging.DEBUG)
session = aiohttp.ClientSession()
async def handler(request):
    current_time = last_build_date = pub_date = time.time()
    anime_id = int(request.match_info['id'])
    async with session.post('https://graphql.anilist.co', data=json.dumps({'query': ANILIST_QUERY, 'variables': {'id': anime_id}}), headers={'content-type': 'application/json', 'accept': 'application/json'}) as resp:
        anime_info = (await resp.json())['data']['Media']
    title = anime_info['title']['english'] or anime_info['title']['romaji']
    items = []
    for i in anime_info['airingSchedule']['nodes']:
        if current_time >= i['airingAt']:
            text = f'{title} - {i["episode"]}'
            print(anime_info['episodes'], i['episode'])
            if anime_info['episodes'] == i['episode']:
                text += ' END'
            items.append(rfeed.Item(title=text, guid=rfeed.Guid(f'ran-{anime_id}-{i["id"]}', False), pubDate=datetime.fromtimestamp(i['airingAt']), link=anime_info['siteUrl']))
            last_build_date = pub_date = i['airingAt']
        else:
            pub_date = i['airingAt']
            break
    feed = rfeed.Feed(title=title, lastBuildDate=datetime.fromtimestamp(last_build_date), pubDate=datetime.fromtimestamp(pub_date), items=reversed(items), link=request.url, description=f'Aired episodes of {title}')
    return aiohttp.web.Response(body=feed.rss().encode(), content_type='application/rss+xml')

app = aiohttp.web.Application()
app.add_routes([
    aiohttp.web.get(r'/{id:\d+}', handler),
    aiohttp.web.get(r'/{id:\d+}.xml', handler),
    aiohttp.web.get(r'/{id:\d+}.rss', handler)
])
aiohttp.web.run_app(app, port=PORT)
